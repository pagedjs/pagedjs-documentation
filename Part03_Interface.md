# Paged.js Interface

It is certainly easier to use paged.js with a rendering where you _actually_ see the pages of your book side by side. For this, you can directly style the `div` pages using classes.

We have provided an example of CSS to do this in an easy way.

Download this CSS file: [interface-01.1](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/ressources/interface-0.1.css)

Link the CSS file to your document:

```HTML
<link href="path/to/file/interface-0.1.css" rel="stylesheet" type="text/css">
```

You can uncomment and modify part of the CSS file:

In the CSS file you can uncomment and modify parts:

- for recto/verso book
- to see and set the baseline
