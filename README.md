# Paged.js documentation



## Part 1: Getting Started with Paged.js [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part01_Getting-Started-with-paged.md) ]

- A quick presentation of paged.js
  - Visual preview and command line version
  - W3C specifications
  - A community
- Running paged.js
  - Access to paged.js script
  - Use a local server
  - Which browser to use?
- Generating a PDF
  - Option 1: with a browser
  - Option 2: with pagedjs-cli
- Using an interface (ToDo)


## Part 2: How Paged.js works  [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part02_how-pagedjs-works.md) ]

* W3C CSS modules
* Support of W3C specifications in browsers
* The paged.js library
* The Chunker: fragment the content
* The Polysher: polyfill the print declarations
* The Previewer: pagined rendering
* DOM modifications



## Part 3: Interface [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part03_Interface.md) ]



## Part 4: Global Layout  [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part04_Global-Layout.md) ]

- Print media query 
- @page rule
- Page size property
- Margin size property
- Page spread or recto/verso
- Page breaks
- Pseudo class selectors for pages
- Crop marks and bleed 
- Code resume of the chapter


## Part 5: Generated Content [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part05_Generated-Content.md) ]

- The content property
- Generated text
- Generated counters
- Generated images
- Generated links
- Generated content for paged media



## Part 6: Generated Content in Margin Boxes [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part06_Generated-Content-in-Margin-Boxes.md) ]

* Margin boxes of a page
* Page Counter
* Named String: classical running headers/footers
  * Select content of string-set
  * Styling named string
* Running elements: headers/footer with specific (complex) content
  * Styling running elements
* Select element of the page for running title/headers
* Delete generated content in blank page
* Styling margin boxes and generated content
  * Default alignement of generated content
  * Applying style on generated content
  * Define width and height of margin boxes
  * Rotate margin boxes
* Rendering of margin boxes with paged.js



## Part 7: Named pages [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part07_Named-paged.md) ]

* Named pages
* Mix page selectors and named pages
* Priority of @page rules


## Part 8: Cross references [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part08_cross-references.md) ]

* Link targeted
* target-counter()
* target-text()
* Debug

## Part 9: Table of content [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/Part09_Table-of-content.md) ]

* Build the elements of the table of content
* Generate page numbers
* Style the table of content



## FAQ [ [link](https://gitlab.pagedmedia.org/tools/pagedjs-documentation/blob/master/FAQ.md) ]