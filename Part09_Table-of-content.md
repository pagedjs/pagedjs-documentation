# Table of content

In the part, we discuss how to build a table of content with script and the function `target-counter()`.

All the files (scripts, CSS, HTML exemples) of this part to create a table of content are available on gitlab: [https://gitlab.pagedmedia.org/tools/experiments/tree/master/table-of-content](https://gitlab.pagedmedia.org/tools/experiments/tree/master/table-of-content).

## Build the elements of the table of content

First, to have a table of content, you need to make HTML lists of the reveleant titles of your document and link to the unique identifier of this element. This part can be done with your own tool/generator but here is an exemple of a script to generate a table of content: 

```js
function createToc(config){
    const content = config.content;
    const tocElement = config.tocElement;
    const titleElements = config.titleElements;
    
    let tocElementDiv = content.querySelector(tocElement);
    let tocUl = document.createElement("ul");
    tocUl.id = "list-toc-generated";
    tocElementDiv.appendChild(tocUl); 

    // add class to all title elements
    let tocElementNbr = 0;
    for(var i= 0; i < titleElements.length; i++){
        
        let titleHierarchy = i + 1;
        let titleElement = content.querySelectorAll(titleElements[i]);    
        

        titleElement.forEach(function(element) {

            // add classes to the element
            element.classList.add("title-element");
            element.setAttribute("data-title-level", titleHierarchy);

            // add id if doesn't exist
            tocElementNbr++;
            idElement = element.id;
            if(idElement == ''){
                element.id = 'title-element-' + tocElementNbr;
            } 
            let newIdElement = element.id;

        });

    }

    // create toc list
    let tocElements = content.querySelectorAll(".title-element");  

    for(var i= 0; i < tocElements.length; i++){
        let tocElement = tocElements[i];
        let tocNewLi = document.createElement("li");
        tocNewLi.classList.add("toc-element");
        tocNewLi.classList.add("toc-element-level-" + tocElement.dataset.titleLevel);
        tocNewLi.innerHTML = '<a href="#' + tocElement.id + '">' + tocElement.innerHTML + '</a>';
        tocUl.appendChild(tocNewLi);  
    }

}
```



Copy this script to a `.js` and link this file to your document.



The table of content need to be generated before that paged.js fragmented the content into pages. It's important. So, you need to register the handler `beforeParsed()` and call the table of content script inside.   

Add this code in the `head` of you html document after the paged.js script:



```html
<script>
  class handlers extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
    }

    beforeParsed(content){          
      createToc({
        content: content,
        tocElement: '#my-toc-content',
        titleElements: [ '.mw-content-ltr h2', 'h3' ]
      });
    }
    
  }
  Paged.registerHandlers(handlers);
</script>
```



**Configuring the script**

`tocElement`: define the id element where the toc list will be create

`titleElements`: array of the title element you want in the toc list. You can add as many as you want and the elements can be classes like `.title-1` or `.my-content h1`





## Generate page numbers



To generate the page numbers of the table of content, use the links to the target anchors titles in the book and the CSS function `target-counter()`.

In your document, each title that appears in the table of content have a special identifier: 

```html
<h1 id="pre-digital_era" class="title-element" data-title-level="h1">Pre-digital era</h1>
<p>Content...</p>
<h1 id="digital_era" class="title-element" data-title-level="h1">Digital era</h1>
<p>Content...</p>
```



In the table of content list, links are used that target anchors to the titles:

```html
<ul id="toc">
  <li><a href="#pre-digital_era">Pre-digital era</a></li>
  <li><a href="#digital_era">Digital era</a></li>
</ul>
```



In the CSS, the `target-counter` property is used in `::before` and `::after` pseudo-elements and set into the `content` property:

```CSS
#toc li a::after{
	content: target-counter(attr(href), page);
  float: right;
}
```



## Style the table of content



Maybe you want some style, counter list and leaders for your table of content. Based on the table of content generated with above, it's an exemple of CSS you can use :



```css
#list-toc-generated{ list-style: none;}

#list-toc-generated .toc-element a::after{
    content: " p. " target-counter(attr(href), page);
    float: right;
}

#list-toc-generated .toc-element-level-1{
    margin-top: 25px;
    font-weight: bold;
}

#list-toc-generated .toc-element-level-2{
    margin-left: 25px;
}


/* target-text(attr(href), before) doesn't work for now, replace with counters (see below)*/
/* #list-toc-generated .toc-element a::before{
    content: target-text(attr(href), before);
} */

/* counters */

#list-toc-generated{ 
    counter-reset: counterTocLevel1; 
}

#list-toc-generated .toc-element-level-1{ 
    counter-increment: counterTocLevel1; 
    counter-reset: counterTocLevel2; 
}

#list-toc-generated .toc-element-level-1::before{ 
    content: counter(counterTocLevel1) ". ";
    padding-right: 5px;
}

#list-toc-generated .toc-element-level-2{ 
    counter-increment: counterTocLevel2; 
}

#list-toc-generated .toc-element-level-2::before{ 
    content: counter(counterTocLevel1) ". " counter(counterTocLevel2) ". ";
    padding-right: 5px;
}

/*  leader() doesn't work for now, replace with the hack bellow */
/* #list-toc-generated .toc-element a::after{
    content: leader('.') " p. " target-counter(attr(href), page);
    float: right;
} */

/* hack for leaders */

#list-toc-generated{
    overflow-x: hidden;
}

#list-toc-generated .toc-element::after{
    content: ".................................................................................................................................................";
    float: left;
    width: 0;
    padding-left: 5px;
    letter-spacing: 2px;
}

#list-toc-generated .toc-element{
    display: flex; 
}

#list-toc-generated .toc-element a::after{
    position: absolute;
    right: 0;
    background-color: white;
    padding-left: 6px;
}

#list-toc-generated .toc-element a{
    right: 0;
}
```



