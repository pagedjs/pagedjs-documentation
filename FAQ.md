# FAQ

**CSS Variables don't work in @page directive (page size and margin size)**

Unfortunately Chromium/Chrome doesn’t understand css-properties when printing. We're looking at options to transform those into fixed values for printing, until Chromium/Chrome supports it. In the meantime, do not use the variables in these declarations by giving fixed values (calculated in advance).

---

**Default fragmentation of paged.js**

Paged.js fragments all the content of your document, which means it will take the `<body>` tag from your document and transform your content into pages.

- If an image don't have the space to fit in a page, it is pushed into the next page.

---

**Mixing page size or page orientation**

Blink (Chromium/Chrome's rendering engine) doesn’t support mixed orientation or mixed size when it comes to print, so you can only set one orientation and one size per file (generated PDF).

The way that paged.js supports landscape and portrait lets you preview on screen, but when it comes to print, you would need to print two different files and merge them. We're looking at options for postprocessing PDF (using Ghostscript) to handle this kind of tasks automatically, but it's not there yet.

---

**Infinite loop in rendering**

If paged.js makes an infinite loop when rendering (generating empty pages or pages with the same content), it's probably because it can't fit an inline element into the page size you defined

Paged.js checks the overflow only horizontally in the page. If you have elements that are larger than the page vertically, you may have rendering problems because paged.js can't get the element into the page. (For the vertical writing direction, it will be the opposite)

This often happens with `<pre>` or `<table>` (`<tr>`, `<th>`) elements. You need to add line breaks, which is possible with css (https://developer.mozilla.org/en-US/docs/Web/CSS/white-space)

---

**Styling div and section elements**

Paged.js modifies the DOM structure by adding some HTML elements to build and render your layout. It adds a lot of `div` elements. When you make your style sheet, we don't recommend that you call `div` and `section` tag names to apply your styles. You may get an unexpected rendering because your styles will apply to unwanted elements. Instead, use custom class to select the items you want.
